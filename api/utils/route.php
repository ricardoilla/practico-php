<?php 

	$ruteo = "lugares";
	$rutas = array(
				array("ruta" => "obtener_lugares", "class" => "Lugar"),
				array("ruta" => "obtener_lugar_por_pais", "class" => "Lugar"),
				array("ruta" => "insertar_lugar", "class" => "Lugar"),
				array("ruta" => "editar_lugar", "class" => "Lugar"),
				array("ruta" => "obtener_lugar_por_coordenadas", "class" => "Lugar"),
				array("ruta" => "obtener_lugar_por_nombre", "class" => "Lugar"),
				array("ruta" => "obtener_usuarios", "class" => "Usuario"),
				array("ruta" => "insertar_usuario", "class" => "Usuario"),
				array("ruta" => "login", "class" => "Usuario"),
				array("ruta" => "is_token", "class" => "Usuario"),
				array("ruta" => "menus", "class" => "Menu"),

			);
	$exist = false;
	if(isset($_GET) && !empty($_GET['ruteo'])){
		$ruteo = $_GET['ruteo'];

		foreach ($rutas as $key => $value) {
			if($value["ruta"] == $ruteo){
				$exist = true;
				$class = $value["class"];
				break;
			}
		}
	}
?>
