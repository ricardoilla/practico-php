<?php 
	$objUsuario = new Usuario();
	$results = array();

	if($ruteo == "obtener_usuarios"){
		$results 	= $objUsuario->obtenerUsuarios($conn);
	}

	if($ruteo == "insertar_usuario"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objUsuario->insertarUsuario($conn, $post);
	}

	if($ruteo == "login"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objUsuario->login($conn, $post["email"], $post["clave"]);
		//$token 		= $results["success"]["token"];
	}

	if($ruteo == "is_token"){
		$token = "";
		if(isset($_GET) && !empty($_GET['token'])){
			$token = $_GET['token'];
		}
		if($token){
			$results 	= $objUsuario->isToken($conn, $token);
			if(empty($results)){
				$results = array("err" => "Token no válido");
			}
		} else {
			$results 	= array("err" => "Token no válido");
		}
	}
?>

