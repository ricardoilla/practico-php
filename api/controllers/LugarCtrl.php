<?php 
	$objLugar = new Lugar();
	$results = array();

	if($ruteo == "obtener_lugares"){
		$results 	= $objLugar->obtenerLugares($conn);
	}

	if($ruteo == "obtener_lugar_por_pais"){
		$pais = "";
		if(isset($_GET) && !empty($_GET['pais'])){
			$pais = $_GET['pais'];
		}
		if($pais){
			$results 	= $objLugar->obtenerLugarPorPais($conn, $pais);
			if(empty($results)){
				$results = array("err" => "Pais no encontrado");
			}
		} else {
			$results 	= array("err" => "Pais no encontrado");
		}
	}

	if($ruteo == "insertar_lugar"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objLugar->insertarLugar($conn, $post);
	}

	if($ruteo == "editar_lugar"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objLugar->editarLugar($conn, $post);
	}

	if($ruteo == "obtener_lugar_por_coordenadas")
	{
		$latitud = 0;
		$longitud = 0;
		if(isset($_GET) && !empty($_GET['latitud']) && !empty($_GET['longitud'])){
			$latitud = $_GET['latitud'];
			$longitud = $_GET['longitud'];
		}
		if($latitud && $longitud){

			$results 	= $objLugar->obtenerLugarPorCoordenadas($conn, $latitud, $longitud);
			if(empty($results)){
				$results = array("err" => "Coordenadas no encontradas");
			}
		} else {
			$results 	= array("err" => "Coordenadas no encontradas");
		}
	}

	if($ruteo == "obtener_lugar_por_nombre")
	{
		$nombre = "";
		if(isset($_GET) && !empty($_GET['nombre'])){
			$nombre = $_GET['nombre'];
		}
		if($nombre){
			$results 	= $objLugar->obtenerLugarPorNombre($conn, $nombre);
			if(empty($results)){
				$results = array("err" => "Lugar ".$nombre." no encontrado");
			}
		} else {
			$results 	= array("err" => "Lugar ".$nombre." no encontrado");
		}
	}
?>

