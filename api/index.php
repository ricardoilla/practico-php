<?php 
	require('config/conn.php');
	$conn = new Connection();

	// RUTEO
	require('utils/route.php');

	if($exist){
		require('classes/'.$class.'.php');
		require('controllers/'.$class.'Ctrl.php');
	} else {
		$results = array("err" => "Ruteo no encontrado");
	}
	
	header('Content-Type: application/json');
	echo json_encode($results);
?>