<?php 
	Class Lugar {

		public function obtenerLugares($conn)
		{
			$sql = "SELECT * FROM lugares WHERE borrado = 0";
			$res = $conn->query($sql);

			return $res;
		}

		public function obtenerLugarPorPais($conn, $pais)
		{
			$sql = "SELECT paises.pais, lugares.nombre, lugares.latitud, lugares.longitud, lugares.direccion
			FROM lugares,paises WHERE lugares.id_pais = paises.id AND paises.pais LIKE '%$pais%'";
			$res = $conn->query($sql);

			return $res;
		}

		public function insertarLugar($conn,$lugar)
		{

			$sql = "   INSERT INTO lugares (id_pais,nombre,latitud,longitud,direccion) 
			VALUES ('$lugar[id_pais]','$lugar[nombre]', '$lugar[latitud]', '$lugar[longitud]','$lugar[direccion]')   ";

			$res = $conn->query($sql);

			if(empty($res)){
				return array("success" => "Lugar ".$lugar["nombre"]." ingresado correctamente.");
			} else {
				return array("err" => "Error al ingresar el lugar.");
			}

		}

		public function editarLugar($conn,$lugar)
		{
			$sql = "UPDATE lugares SET id_pais = '$lugar[id_pais]', nombre = '$lugar[nombre]', latitud = '$lugar[latitud]', longitud = '$lugar[longitud]', direccion = '$lugar[direccion]' 
			WHERE id='$lugar[id]'";
			$res = $conn->query($sql);

			if(empty($res)){
				return array("success" => "Lugar ".$lugar["nombre"]." actualizado correctamente.");
			} else {
				return array("err" => "Error al actualizar el lugar.");
			}
		}

		public function obtenerLugarPorCoordenadas($conn, $latitud, $longitud)
		{

			$sql = "SELECT * FROM lugares WHERE latitud = '$latitud' AND longitud = '$longitud' ";
			$res = $conn->query($sql);
			return $res;
		}

		public function obtenerLugarPorNombre($conn, $nombre)
		{
			$sql = "SELECT * FROM lugares WHERE nombre LIKE '%$nombre%'";
			$res = $conn->query($sql);
			return $res;
		}
	}
?>


