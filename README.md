# Practico PHP

#API: ABM de Lugares

- Listar lugares (id, id_pais, nombre, lat, lon, direccion, borrado)
- Obtener lugar por país
- Tabla pais (id, nombre)
- Crear / Editar lugares
- Buscador de lugares por coordenadas (lat, lon)
- Buscador por nombre

#PARTE 2 (agregado):

- Crear tabla usuarios (nombre, email , clave)
- Listar Usuarios
- Crear nuevo usuario
- Login usuario